<?php

namespace Rooslunn\Upkindle;

require_once 'vendor/autoload.php';
require_once 'settings.php';

use \Mailjet;

function testedBody(): array {
    return [
        'Messages' => [
            [
                'From' => [
                    'Email' => 'rooslunn.kindle@gmail.com',
                    'Name' => 'Kindle',
                ],
                'To' => [
                    [
                        'Email' => 'rooslunn.kindle_59a2e8@kindle.com',
                        'Name' => 'Kindle',
                    ]
                ],
                'Subject' => 'Feed Receiver',
                'TextPart' => 'Dear passenger, welcome to aboard! May the delivery force be with you!',
            ]
        ]
    ];
}

$from_name = 'Kindle';
$to_name = 'Kindle';

$From = [
    'Email' => $from_aliases['kindle'],
    'Name' => $from_name,
]; 

$To = [[
    'Email' => $to_aliases['kindle'],
    'Name' => $to_name,
]];

$Subject = 'Kindle';

$files = [
    'test/mcdonalds_1953-menu.webp', 
    'test/we-are-sorry-to-see-you-go.gif'
];

$Attachments = [];

foreach ($files as $file) {
    $contentType = get_mime_content_type($file);
    $filename = $file;
    $base64Content = base64_encode(base64_encode(file_get_contents($file)));
    $Attachments[] = compact('contentType', 'filename', 'base64Content');
}

$TextPart = 'Dear receiver! Get attachments, please';

$Messages = [compact('From', 'To', 'Subject', 'TextPart', 'Attachments')];
$body = compact('Messages');

// Send email via MJ
$mj = new Mailjet\Client($mailjet['api_key'], $mailjet['secret_key'], true, ['version' => 'v3.1']);

try {
    $response = $mj->post(Mailjet\Resources::$Email, compact('body'));
    // $response->success() && var_dump($response->getData());
    $response->success() && (print('Success' . PHP_EOL));
} catch(\Throwable $e) {
    echo $e->getMessage(), PHP_EOL;
}
