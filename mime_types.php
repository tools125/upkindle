<?php

require_once 'helpers.php';


define('APACHE_MIME_TYPES_URL', 
    'http://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types');

define('APACHE_MIME_TYPES_COMMENT', '#');


function get_latest_mime_types(): array {
    $result = [];

    $url = constant('APACHE_MIME_TYPES_URL');
    $content = file_get_contents($url);

    $exploded = explode("\n", $content);

    $uncommented = array_filter($exploded, function (string $item) {
        return !empty($item) && !str_starts_with($item, constant('APACHE_MIME_TYPES_COMMENT'));
    });

    $pairs = array_map(function (string $item) {
        $parted = explode(' ', preg_replace('/\s+/', ' ', $item));
        $parted = array_values(array_filter($parted));
        [$value, $key] = $parted;
        return compact('key', 'value');
    }, $uncommented);

    foreach($pairs as $pair) {
        $result[$pair['key']] = $pair['value'];
    }

    return $result;
}

define('MIME_TYPES_SERVER_KEY', 'latest_mime_types');

$_SERVER[constant('MIME_TYPES_SERVER_KEY')] = get_latest_mime_types();

function get_mime_content_type(string $filename): string {
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    return $_SERVER['latest_mime_types'][$ext] ?? '';
}

// var_dump(get_latest_mime_types());
