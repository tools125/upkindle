<?php

$from_aliases = [
    'kindle' => 'rooslunn@gmail.com',
];

$to_aliases = [
    'kindle' => 'rooslunn.kindle_59a2e8@kindle.com',
];

$mailjet = [
    'type' => 'Main Account',
    'api_key' => 'Your mailjet api key',
    'secret_key' => 'Your mailjet secret key',
];

return compact('from_aliases', 'to_aliases');